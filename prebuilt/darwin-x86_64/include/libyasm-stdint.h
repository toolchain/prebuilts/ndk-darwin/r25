#ifndef _YASM_LIBYASM_STDINT_H
#define _YASM_LIBYASM_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "yasm 1.3.0"
/* generated using /Volumes/Android/buildbot/src/android/ndk-r25-release/prebuilts/clang/host/darwin-x86/clang-r450784d1/bin/clang --target=x86_64-apple-darwin -mmacosx-version-min=10.9 -DMACOSX_DEPLOYMENT_TARGET=10.9 -isysroot/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk -Wl,-syslibroot,/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk -mlinker-version=711 -Os -fomit-frame-pointer -w -s */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
